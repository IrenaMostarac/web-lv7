var mongoose = require('mongoose');

var projectSchema = new mongoose.Schema({  
  naziv: String,
  opis: String,
  cijena: Number,
  datump: { type: Date, default: Date.now },
  datumz: { type: Date, default: Date.now },
  poslovi: String,
  vlasnik: String,
  members: [mongoose.Schema.Types.ObjectId],
  arhiva: { type: Boolean, default: false }
});
mongoose.model('Project', projectSchema);