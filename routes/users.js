var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

const User = mongoose.model('User');
const bcrypt = require('bcryptjs');
const passport = require('passport');

//Users index
router.route('/')
  .get(function (req, res, next) {
    res.render('users/index');
  });

//Users - login
router.route('/login')
  .get(function (req, res, next) {
    if(req.user==null)
      res.render('users/login');
    else
      res.redirect('/projects');
  })
  .post(function(req, res, next){
    passport.authenticate('local', {
      successRedirect: '/projects',
      failureRedirect: '/users/login',
      failureFlash: true
    })(req, res, next);
  });

//Users - register
router.route('/register')
  .get(function (req, res, next) {
    if(req.user==null)
      res.render('users/register');
    else
      res.redirect('/projects');
  })
  .post(function(req, res){
    var name = req.body.name;
    var email = req.body.email;
    var pass = req.body.password;
    var pass2 = req.body.confirmPassword;
    let errors = [];

    //Validation
    if(!name || !email || !pass || !pass2)
      errors.push({msg:'Potrebno je ispuniti sva polja!'});
    if(pass!==pass2)
      errors.push({msg: 'Unesene lozinke se ne podudaraju'});
    if(pass.length < 6)
      errors.push({msg: 'Lozinka treba sadržavati najmanje 6 znakova!'});

    //Check errors
    if(errors.length>0){
      res.render('users/register', {errors, name, email});
    }
    else{
      //Register user
      User.findOne({email: email})
        .then(user => {
          if(user){
            errors.push({msg: 'Uneseni korisnik ('+ email +') je već registriran!'});
            res.render('users/register', {errors, name, email});
          }
          else{ //Save user to db
            var newUser = new User({
              name: name,
              email: email,
              password: pass
            });
            
            //hash the password
            bcrypt.genSalt(10, (err, salt)=> 
              bcrypt.hash(newUser.password, salt, (err, hash) => {
                if(err) throw err;
                newUser.password = hash;
                newUser.save()
                  .then(user => {
                    req.flash('success_msg', 'Uspješna registracija! Prijava je sada moguća!')
                    res.redirect('login');
                  })
                  .catch(err => console.log(err));
              }))
          }
        })
    }
  });

router.route('/logout')
  .get(function(req, res, next){
    req.logout();
    req.flash('success_msg', 'Uspješna odjava!');
    res.redirect('/users/login');
  });

module.exports = router;
