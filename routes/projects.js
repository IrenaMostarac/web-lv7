var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

const { ensureAuthenticated } = require('../config/auth');

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

//build the REST operations at the base for projects
//this will be accessible from http://127.0.0.1:3000/projects if the default route for / is left unchanged
router.route('/')
    //GET all projects
    .get(ensureAuthenticated, function (req, res, next) {
        //user is project owner
        mongoose.model('Project').find({
            vlasnik: req.user._id,
            arhiva: false
        }, function (err, myprojects) {
            if (err) {
                return console.error(err);
            } else {
                //user is project member
                mongoose.model('Project').find({
                    members: req.user._id,
                    arhiva: false
                }, function(err, projects){
                    if(err)
                        return console.error(err)
                    else{
                        //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                        res.format({
                            //HTML response will render the index.jade file in the views/projects folder. We are also setting "projects" to be an accessible variable in our jade view
                            html: function () {
                                res.render('projects/index', {
                                    title: 'Moji projekti',
                                    username: req.user.name,
                                    myprojects: myprojects,
                                    projects: projects
                                });
                            },
                            //JSON response will show all projects in JSON format
                            json: function () {
                                res.json(projects);
                            }
                        });
                    }
                })
            }
        });
    })
    //POST a new project
    .post(function (req, res) {
        // Get values from POST request. These can be done through forms or REST calls. These rely on the "name" attributes for forms
        var naziv = req.body.naziv;
        var opis = req.body.opis;
        var cijena = req.body.cijena;
        var datump = req.body.datump;
        var datumz = req.body.datumz;
        var poslovi = "";
        var vlasnik = req.user._id;

        //call the create function for our database
        mongoose.model('Project').create({
            naziv: naziv,
            opis: opis,
            cijena: cijena,
            datump: datump,
            datumz: datumz,
            poslovi: poslovi,
            vlasnik: vlasnik
        }, function (err, project) {
            if (err) {
                res.send(err);
            } else {
                //Project has been created
                console.log('POST creating new project: ' + project);
                res.format({
                    //HTML response will set the location and redirect back to the home page. You could also create a 'success' page if that's your thing
                    html: function () {
                        // If it worked, set the header so the address bar doesn't still say /adduser
                        res.location("projects");
                        // And forward to success page
                        res.redirect("/projects");
                    },
                    //JSON response will show the newly created project
                    json: function () {
                        res.json(project);
                    }
                });
            }
        })
    });

/* GET New project page. */
router.get('/new', ensureAuthenticated, function (req, res) {
    res.render('projects/new', { title: 'Dodaj novi projekt' });
});

// route middleware to validate :id
router.param('id', function (req, res, next, id) {
    //console.log('validating ' + id + ' exists');
    //find the ID in the Database
    mongoose.model('Project').findById(id, function (err, project) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(id + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function () {
                    next(err);
                },
                json: function () {
                    res.json({ message: err.status + ' ' + err });
                }
            });
            //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
            //console.log(project);
            // once validation is done save the new item in the req
            req.id = id;
            // go to the next thing
            next();
        }
    });
});

router.route('/arhiva')
    .get(ensureAuthenticated, function (req, res, next) {
        //user is owner of projects
        mongoose.model('Project').find({
            vlasnik: req.user._id,
            arhiva: true
        }, function (err, myprojects) {
            if (err) {
                return console.error(err);
            } else {
                //user is member on projects
                mongoose.model('Project').find({
                    members: req.user._id,
                    arhiva: true
                }, function(err, projects){
                    if(err)
                        return console.error(err)
                    else{
                        res.render('projects/arhiva', {
                            title: 'Arhiva projekata',
                            myprojects: myprojects,
                            projects: projects
                        });
                    }
                })
            }
        });
    });

router.route('/arhiva/:id')
    .put(function(req, res){
        var arhiva = false;
        mongoose.model('Project').findById(req.id, function(err, project){
            if(err)
                return console.error(err);
            else{
                project.update({
                    arhiva: arhiva
                }, function(err, projectID){
                    if(err)
                        console.error(err);
                    else{
                        res.redirect('/projects/arhiva');
                    }
                })
            }
        })
    });

router.route('/:id')
    .get(ensureAuthenticated, function (req, res) {
        mongoose.model('Project').findById(req.id, function (err, project) {
            if (err) {
                console.log('GET Error: There was a problem retrieving: ' + err);
            } else {
                console.log('GET Retrieving ID: ' + project._id);
                
                res.format({
                    html: function () {
                        res.render('projects/show', {
                            title: project.naziv,
                            "project": project
                        });
                    },
                    json: function () {
                        res.json(project);
                    }
                });
            }
        });
    });

router.route('/:id/edit')
    //GET the individual project by Mongo ID
    .get(ensureAuthenticated, function (req, res) {
        //search for the project within Mongo
        mongoose.model('Project').findById(req.id, function (err, project) {
            if (err) {
                console.log('GET Error: There was a problem retrieving: ' + err);
            } else {
                //Return the project
                console.log('GET Retrieving ID: ' + project._id);
                res.format({
                    //HTML response will render the 'edit.jade' template
                    html: function () {
                        res.render('projects/edit', {
                            title: project.naziv,
                            "project": project
                        });
                    },
                    //JSON response will return the JSON output
                    json: function () {
                        res.json(project);
                    }
                });
            }
        });
    })
    //PUT to update a project by ID
    .put(function (req, res) {
        // Get our REST or form values. These rely on the "naziv" attributes
        var naziv = req.body.naziv;
        var opis = req.body.opis;
        var cijena = req.body.cijena;
        var datump = req.body.datump;
        var datumz = req.body.datumz;
        var poslovi = req.body.poslovi;
        var arhivaChk = req.body.arhiva;
        var arhiva;
        if(arhivaChk == "on"){
            arhiva = true;
        }
        else {
            arhiva = false;
        }

        //find the document by ID
        mongoose.model('Project').findById(req.id, function (err, project) {
            //update it
            project.update({
                naziv: naziv,
                opis: opis,
                cijena: cijena,
                datump: datump,
                datumz: datumz,
                poslovi: poslovi,
                arhiva: arhiva
            }, function (err, projectID) {
                if (err) {
                    res.send("There was a problem updating the information to the database: " + err);
                }
                else {
                    //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                    res.format({
                        html: function () {
                            res.redirect("/projects/");
                        },
                        //JSON responds showing the updated values
                        json: function () {
                            res.json(project);
                        }
                    });
                }
            })
        });
    })
    //DELETE a Project by ID
    .delete(function (req, res) {
        //find Project by ID
        mongoose.model('Project').findById(req.id, function (err, project) {
            if (err) {
                return console.error(err);
            } else {
                //remove it from Mongo
                project.remove(function (err, project) {
                    if (err) {
                        return console.error(err);
                    } else {
                        //Returning success messages saying it was deleted
                        console.log('DELETE removing ID: ' + project._id);
                        res.format({
                            //HTML returns us back to the main page, or you can create a success page
                            html: function () {
                                res.redirect("/projects");
                            },
                            //JSON returns the item with the message that is has been deleted
                            json: function () {
                                res.json({
                                    message: 'deleted',
                                    item: project
                                });
                            }
                        });
                    }
                });
            }
        });
    });

router.route('/:id/member-edit')
    //GET the individual project by Mongo ID
    .get(ensureAuthenticated, function (req, res) {
        //search for the project within Mongo
        mongoose.model('Project').findById(req.id, function (err, project) {
            if (err) {
                console.log('GET Error: There was a problem retrieving: ' + err);
            } else {
                //Return the project
                console.log('GET Retrieving ID: ' + project._id);
                res.render('projects/member-edit', {
                            title: project.naziv,
                            "project": project
                        });
            }
        });
    })
    //PUT to update a project by ID
    .put(function (req, res) {
        // Get our REST or form values. These rely on the "naziv" attributes
        var poslovi = req.body.poslovi;
        //find the document by ID
        mongoose.model('Project').findById(req.id, function (err, project) {
            //update it
            project.update({
                poslovi: poslovi
            }, function (err, projectID) {
                if (err) {
                    res.send("There was a problem updating the information to the database: " + err);
                }
                else {
                    //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                    res.format({
                        html: function () {
                            res.redirect("/projects/");
                        },
                        //JSON responds showing the updated values
                        json: function () {
                            res.json(project);
                        }
                    });
                }
            })
        });
    })

router.route('/:id/members')
    .get(ensureAuthenticated, function (req, res, next) {
        mongoose.model('Project').findById(req.id, function (err, project) {
            if (err) {
                return console.error(err);
            } else {
                var members = project.members;
                mongoose.model('User').find(
                    {_id: {
                            $in : members
                        }
                    },
                    function(err, users){
                    if(err)
                        return console.error(err);
                    else{
                        console.log(users);
                        res.render('projects/members', {
                            title: project.naziv,
                            "project": project,
                            "members": users
                        });
                    }
                })
            }
        })
    })
    .put(function (req, res) {
        // Get our REST or form values. These rely on the "naziv" attributes
        var email = req.body.email;
        mongoose.model('User').findOne({email:email})
        .then(user => {
            if(user){
                mongoose.model('Project').findById(req.id, function (err, project) {
                    var members = project.members;
                    members.push(mongoose.Types.ObjectId(user._id));
                    project.update({
                        members: members
                    }, function (err, projectID) {
                        if (err) {
                            res.send("There was a problem updating the information to the database: " + err);
                        }
                        else {
                            res.redirect("/projects/"+req.id+"/members/");
                        }
                    })
                });
            }
            else{
                req.flash('error_msg', 'Uneseni korisnik nije registriran!')
                res.redirect("/projects/"+req.id+"/members/new");
            }
        })
    });

router.param('memberId', function (req, res, next, memberId) {
        req.memberId = memberId;
        next();
});

router.route('/:id/members/:memberId')
    .put(function (req, res) {
        //find the document by ID
        mongoose.model('Project').findById(req.id, function (err, project) {
            var members = project.members;
            console.log(req.memberId);
            members.pull(mongoose.Types.ObjectId(req.memberId));
            project.update({
                members: members
            }, function (err, projectID) {
                if (err) {
                    res.send("There was a problem updating the information to the database: " + err);
                }
                else {
                    //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                    res.format({
                        html: function () {
                            res.redirect("/projects/"+req.id+"/members/");
                        },
                        //JSON responds showing the updated values
                        json: function () {
                            res.json(project);
                        }
                    });
                }
            })
        });
    });

/* GET New member page. */
router.get('/:id/members/new', ensureAuthenticated, function (req, res) {
    res.render('projects/members/new', {
        title: 'Dodaj novog člana',
        id: req.id
    });
});

module.exports = router;